﻿using System;

namespace AplicacionMatematica
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Console.WriteLine("Bienvenido!, Que operacion quiere realizar?: ");

            Console.WriteLine("1- Mostrar tabla de multiplicar. ");
            Console.WriteLine("2- Suma. ");
            Console.WriteLine("3- Resta. ");
            Console.WriteLine("4- Multiplicacion. ");
            Console.WriteLine("5- Division. ");

            int num = Convert.ToInt32(Console.ReadLine());
            
            if (num == 1) {
                
                Console.WriteLine("Ingrese un numero: ");
                int a = Convert.ToInt32(Console.ReadLine());
                    for (int i = 0; i <= 10; i++) {
                        int res = a * i;
                    Console.WriteLine(a + "x" + i + "=" + res);
                }
            }

            if (num == 2)
            {
                Console.WriteLine("Ingrese primer numero: ");
                int a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ingrese segundo numero: ");
                int b = Convert.ToInt32(Console.ReadLine());
                int res = a + b;
                Console.WriteLine( a + "+"+ b+ "="+ res);
            }

            if (num == 3)
            {
                Console.WriteLine("Ingrese primer numero: ");
                int a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ingrese segundo numero: ");
                int b = Convert.ToInt32(Console.ReadLine());
                int res = a - b;
                Console.WriteLine(a + "-" + b + "=" + res);
            }

            if (num == 4)
            {
                Console.WriteLine("Ingrese primer numero: ");
                int a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ingrese segundo numero: ");
                int b = Convert.ToInt32(Console.ReadLine());
                int res = a * b;
                Console.WriteLine(a + "x" + b + "=" + res);
            }
            if (num == 5)
            {
                Console.WriteLine("Ingrese primer numero: ");
                int a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ingrese segundo numero: ");
                int b = Convert.ToInt32(Console.ReadLine());
                int res = a / b;
                Console.WriteLine(a + "/" + b + "=" + res);
            }

            if (num>5 || num<1)
            {
                Console.WriteLine("Opcion Invalida.");
            }
        }
    }
}
